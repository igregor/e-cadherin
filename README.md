## E-Cadherin

## Description
This repository provides raw data and files for evaluation of the data described in the paper: "Observation of E-cadherin Adherens Junction Dynamics with Metal-Induced Energy Transfer Imaging and Spectroscopy" by Tao Chen and Jörg Enderlein.


## Usage
The files require the software packages MatLab (www.mathworks.com) and Mathematica (www.wolfram.com).

## Support
For further help or assistance, please contact Jörg Enderlein (jenderl@gwdg.de).


## Authors and acknowledgment
This data and software is the work of Dr. Tao Chen (tao.chem@phys.uni-goettingen.de) and Prof. Jörg Enderlein (jenderl@gwdg.de).

The work was conducted at the Third Institute of Physics at the Georg-August-University at Göttingen, Germany.
