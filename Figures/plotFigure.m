% plot figure for Figure 3d,3e,3f
load('Figure 3d.mat'); % load the data file
z1(z1==0) = NaN;
pcolor(xh,yh,z1); 
set(gcf,'Position',[100 100 500 600]);
set(get(gca,'children'),'edgecolor','none'); colorbar;
caxis([0 10])
colormap jet;
xlabel('Height (nm)'); ylabel('Counts per 100 ms');

set(gca,'XTick',20:20:80);
set(gca,'YTick',2e4:1e4:4.5e4);
set(gca,'FontSize',24,'Fontname','Arial','linewidth',2);

%% plot figure for Figure 4c
load('Figure 4_off_on_data.mat');
load('Figure 4c_fit.mat');
t = linspace(0,1,25);
[x,y] = mHist(out_high_t,t);
[x1,y1] = mHist(out_low_t,t);
y2 = [x/sum(x) x1/sum(x1)];
bar(t,y2(:,1),'FaceAlpha',0.5,'FaceColor',[1, 0.6, 0.2]);
hold on
bar(t,y2(:,2),'FaceAlpha',0.5,'FaceColor',[0.6, 0.8, 0.2]);
plot(fit1(:,1),fit1(:,2),'Color',[1, 0.6, 0.2]);
plot(fit1(:,3),fit1(:,4),'Color',[0.6, 0.8, 0.2]);
xlim([-0.020 0.5]);
ylim([0,0.55]);
xlabel('waiting time (s)');
ylabel('pdf');
set(gca,'XTick',[0,0.2,0.4],'YTick',[0,0.2,0.4],'FontSize',24)
%% plot figure for Figure 4d,4e,4f
load('Figure 4_off_on_data.mat'); % load the off-on raw data file

%------------------------- p(i)*p(j)
data = out_low_t;  % out_low_t is low state waiting times & out_high_t is the high state waiting times
tt = 1;                    % change tt for the time lag
% data = out_high_t; tt =1; % for Figure 4d
% data = out_low_t; tt =1; % for Figure 4e
% data = out_low_t; tt =300; % for Figure 4f
nn = zeros(1,tt);   % shift  tn+nn
data1 = [nn data];             
data1 = data1(1:end-tt);  % tn+1
xa =  linspace(0.01, 0.25, 20);
yv =  xa;   
[z, xv, yv] = mHist2(data,data1,xa,yv); % z is the p(i)*p(j)

%----------------------- p(i) \times p(j)--------------------
p1 = mHist(data,xa);
p2 = mHist(data1,xa);
zz = p1*p2'/(length(data)-1);
%----------------------- tn - tn+nn ------------------------
dif_z = z-zz;
%--------------------plot----------------------------
pcolor(xa,yv,dif_z); 
axis image; colorbar; 
set(get(gca,'children'),'edgecolor',[0.1 0.1 0.1]);
caxis([-2 5]); colormap(CustomColormap2)
xlabel('waiting time t_l_,_n (s)'); ylabel('waiting time t_l_,_n_+_2_0 (s)');
set(gca,'XTick',0:0.05:0.5);
set(gca,'YTick',0:0.05:0.5);
set(gca,'FontSize',24,'linewidth',0.7);
hh = colorbar;hh.Ticks = [-5:5:20];
POS = get(hh,'pos');POS = POS + [+0.015 -0.00 0.01  0];set(hh,'pos',POS)